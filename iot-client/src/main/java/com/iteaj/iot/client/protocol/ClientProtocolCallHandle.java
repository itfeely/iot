package com.iteaj.iot.client.protocol;

import com.iteaj.iot.FreeProtocolHandle;

public interface ClientProtocolCallHandle extends FreeProtocolHandle<ClientInitiativeProtocol> {

    @Override
    default Object handle(ClientInitiativeProtocol protocol) {
        this.doHandle(protocol);
        return null;
    }

    void doHandle(ClientInitiativeProtocol protocol);
}
