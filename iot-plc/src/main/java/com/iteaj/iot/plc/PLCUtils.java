package com.iteaj.iot.plc;

public class PLCUtils {

    /**
     * 根据字符串内容，获取当前的位索引地址，例如输入 6,返回6，输入15，返回15，输入B，返回11
     * @param bit 位字符串
     * @return 结束数据
     */
    public static int calculateBitStartIndex( String bit ) {
        if (bit.contains("A") || bit.contains("B") || bit.contains("C") || bit.contains("D") || bit.contains("E") || bit.contains("F")) {
            return Integer.parseInt(bit, 16);
        } else {
            return Integer.parseInt(bit);
        }
    }

    /**
     * 复制数组内容
     * @param value 数组
     * @param index 起始索引
     * @param length 数据的长度
     * @return 新的数组值
     */
    public static boolean[] copeArray(boolean[] value, int index, int length){
        if (value == null) return null;
        boolean[] buffer = new boolean[Math.min( value.length, length )];
        System.arraycopy( value, index, buffer, 0, buffer.length );
        return buffer;
    }

    /**
     * 将byte数组转换为bool[]数据，不为指定的书 则表示为 True，反之为 False
     * @param data 等待转换的数据信息
     * @param compare 0为false 1为true
     * @return 转换结果
     */
    public static boolean[] byteToBoolArray(byte[] data, byte compare ){
        if(data == null) return  null;
        boolean[] array = new boolean[data.length];
        for(int i = 0; i< data.length; i++){
            array[i] =  data[i] != compare;
        }
        return array;
    }

    /**
     * 从Byte数组中提取所有的位数组
     * @param content 原先的字节数组
     * @return 转换后的bool数组
     */
    public static boolean[] byteToBoolArray(byte[] content){
        return content == null ? null : byteToBoolArray(content, content.length * 8);
    }

    /**
     * 从Byte数组中提取位数组，length代表位数
     * @param InBytes 原先的字节数组
     * @param length 想要转换的长度，如果超出自动会缩小到数组最大长度
     * @return 结果对象
     */
    public static boolean[] byteToBoolArray(byte[] InBytes, int length) {
        if (InBytes == null) return null;

        if (length > InBytes.length * 8) length = InBytes.length * 8;
        boolean[] buffer = new boolean[length];

        for (int i = 0; i < length; i++) {
            buffer[i] = BoolOnByteIndex(InBytes[i / 8], i % 8);
        }

        return buffer;
    }

    /**
     * 获取byte数据类型的第offset位，是否为True<br />
     * @param value byte数值
     * @param offset 索引位置
     * @return 结果
     */
    public static boolean BoolOnByteIndex(byte value, int offset ) {
        byte temp = GetDataByBitIndex(offset);
        return (value & temp) == temp;
    }

    private static byte GetDataByBitIndex( int offset ) {
        switch (offset) {
            case 0:
                return 0x01;
            case 1:
                return 0x02;
            case 2:
                return 0x04;
            case 3:
                return 0x08;
            case 4:
                return 0x10;
            case 5:
                return 0x20;
            case 6:
                return 0x40;
            case 7:
                return (byte) 0x80;
            default:
                return 0;
        }
    }
}
